Your mission, should you choose to accept it:
- [ ] Create a new and separate user (e.g. `@username-admin`) on GitLab.com. Please use your GitLab.com email as the email, e.g. `username+admin@gitlab.com`.
- [ ] Set a strong (and different) password on the account, store it in 1Password
- [ ] Enable 2FA
- [ ] Tag your manager when the above are complete

Once the approvers below have checked their boxes, your manager will upgrade the account.
- [ ] @lyle

/confidential